
import java.util.Scanner;

public class Console {

    public static void main(String[] args) {

        Scanner leitor = new Scanner(System.in);

        boolean executar = true;
        int contador = 0;
        String[] letras = new String[] { "A", "B", "C" };

        System.out.println("Posição 1: " + letras[0]);

        for (int i = 0; i < 3; i++) {
            System.out.println("Letra: " + letras[i]);
        }

        while (executar && contador < letras.length) {
            System.out.println("Letra: " + letras[contador]);
            contador++;
        }

        // System.out.println("Digite a nota A:");
        // int a = leitor.nextInt();
        // System.out.println("Digite a nota B:");
        // var b = leitor.nextInt();
        // double resultado = (a + b) / 2;

        // System.out.println("Resultado: " + resultado);

        // if (resultado >= 6) {
        // System.out.println("Aprovado!");
        // } else if (resultado > 1) {
        // System.out.println("Recuperação!");
        // } else {
        // System.out.println("Reprovado!");
        // }

        // int resultadoInteiro = (int) resultado; // casting
        // var resultadoDinamico = a + b;
        // var str = "É um texto?";
        // var resultadoString = a + b;

        // System.out.println("resultadoInteiro: " + resultadoInteiro);
        // System.out.println("resultadoDinamico: " + resultadoDinamico);
        // System.out.println("str: " + str);
        // System.out.println("resultadoString: " + resultadoString);

        // System.out.println("Digite um texto...");
        // String texto = leitor.nextLine();
        // System.out.println("Texto digitado: " + texto);

        // System.out.println("Digite um número inteiro...");
        // int inteiro = leitor.nextInt();
        // System.out.println("Número digitado: " + inteiro);

        // System.out.println("Digite um número decimal...");
        // double numero = leitor.nextDouble();
        // System.out.println("Número digitado: " + numero);

        leitor.close();
    }

}
